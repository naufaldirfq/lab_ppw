from django.shortcuts import render

# Create your views here.
from django.http import HttpResponse

def index(request):
	return render(request, 'Home.html')

def home(request):
	return render(request, 'Home.html')

def about(request):
	return render(request, 'About.html')

def experience(request):
	return render(request, 'Experience.html')

def project(request):
	return render(request, 'Project.html')

def form(request):
	return render(request, 'Form.html')