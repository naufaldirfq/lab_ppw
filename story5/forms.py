from django import forms
import datetime
class Add_Project_Form(forms.Form):
    error_messages = {
        'required': 'Please type',
    }
    position_attrs = {
        'type': 'text',
        'class': 'todo-form-input',
        'placeholder':'Nama Kegiatan'
    }

    date_attrs = {
        'type': 'date',
        'class': 'todo-form-input',
    }
    title_attrs = {
        'type': 'text',
        'class': 'todo-form-input',
        'placeholder':'Tempat Kegiatan'
    }
    description_attrs = {
        'type': 'text',
        'cols': 50,
        'rows': 4,
        'class': 'todo-form-textarea',
        'placeholder':'Deskripsi Kegiatan'
    }
    OPTION = {
        ('Akademis', 'Akademis'),
        ('Kepanitiaan', 'Kepanitiaan'),
        ('Organisasi', 'Organisasi'),
        ('Lainnya', 'Lainnya')
    }


    position = forms.CharField(label='Nama Kegiatan', required=True, max_length=27, widget=forms.TextInput(attrs=position_attrs))
    title = forms.CharField(label='Tempat Kegiatan', required=True, max_length=27, widget=forms.TextInput(attrs=title_attrs))
    description = forms.CharField(label='Deskrpsi Kegiatan', required=True, widget=forms.Textarea(attrs=description_attrs))
    tanggal = forms.DateField(required=True,initial=datetime.date.today, widget = forms.DateInput(attrs = {'type' : 'date'}))
    kategori = forms.ChoiceField(choices = OPTION)
    time = forms.TimeField(widget = forms.TimeInput(attrs = {'type' : 'time'}))