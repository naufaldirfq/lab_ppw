# Generated by Django 2.1.1 on 2018-10-03 03:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('story5', '0006_auto_20181003_1001'),
    ]

    operations = [
        migrations.AlterField(
            model_name='project',
            name='date',
            field=models.DateField(auto_now_add=True),
        ),
    ]
