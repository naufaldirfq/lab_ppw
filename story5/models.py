from django.db import models
from django.utils import timezone
from datetime import date
# Create your models here.
class Project(models.Model):
	position = models.CharField(max_length=200)
	title = models.CharField(max_length=200)
	description = models.TextField()
	tanggal = models.DateField(default=date.today)
	kategori = models.CharField(max_length = 200)
	time = models.TimeField(auto_now = False, auto_now_add = False)
