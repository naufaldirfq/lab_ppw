from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Add_Project_Form
from .models import Project

response = {}

# Create your views here.
def index(request):
	project = Project.objects.all()
	response['project'] = project

	html = 'Home.html'
	return render(request, html, response)

def addproject(request):
	response['title'] = 'Add Project'
	response['addproject_form'] = Add_Project_Form

	html = 'addproject.html'

	return render(request, html, response)

def saveproject(request):
	form = Add_Project_Form(request.POST or None)
	if(request.method == 'POST' and form.is_valid()):
		response['position'] = request.POST['position']
		response['title'] = request.POST['title']
		response['description'] = request.POST['description']
		response['tanggal'] = request.POST.get('tanggal')
		response['kategori'] = request.POST['kategori']
		response['time'] = request.POST['time']
		project = Project(position=response['position'], title=response['title'],description=response['description'], tanggal = response['tanggal'],kategori= response['kategori'], time = response['time'])
		project.save()

		project = Project.objects.all()
		response['project'] = project
		html = 'table.html'
		return render(request, html, response)
	else:
		html = 'table.html'
		return render(request, html, response)

def delete_every(self):
    deleteproject = Project.objects.all().delete()
    response['deleteproject'] = deleteproject
    html = 'table.html'
    return render(self, html, response)

# def message_table(request):
#         project = Project.objects.all()
#         response['project'] = project
#         html = 'story5/table.html'
#         return render(request, html , response)
