from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('addproject/', views.addproject, name='addproject'),
    path('saveproject/', views.saveproject, name='saveproject'),
    path('delete_every/', views.delete_every, name='delete_every'),
]