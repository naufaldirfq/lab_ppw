"""lab4 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
from story4.views import home as page1
from story4.views import about as page2
from story4.views import experience as page3
from story4.views import project as page4
from story4.views import form as page5
from django.urls import path, include
from django.urls import re_path, include

urlpatterns = [
    path('admin/', admin.site.urls),
        path('', include(('story5.urls', 'story5'), namespace="story5")),
   	re_path(r'^$', page1, name= "home"),
    re_path(r'^home', page1, name="home"),  
 	re_path(r'^about', page2, name="about"),
 	re_path(r'^experience', page3, name="experience"),
 	re_path(r'^project', page4, name="project"),
 	re_path(r'^form', page5, name="form")

]